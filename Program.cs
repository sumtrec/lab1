﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace lab20_06_20
{
    class Program
    {
        static Dictionary<int, Person> phoneList = new Dictionary<int, Person>();

        static void Main(string[] args)
        {
           // string name="", surname = "", secondName = "";
            Console.WriteLine("Что Вы хотите сделать?\nНомера задач:\n1. Добавить новый контакт.\n" +
                "2. Удалить старый контакт.\n" +
                "3. Вывести всю записную книжку.\n" +
                "4. Вывести контакт по номеру в книжке.\n" +
                "5. Вывести данные контакта по Фамилии и Имени.\n" +
                "6. Выйти(закрыть программу).");
            string check = "";
            int reading = '6';
            int keyy;
            Person person;
            do
            {
                reading = int.Parse(Console.ReadLine());
                if (reading == 1 || reading == 2 || reading == 3 || reading == 4 || reading == 5)
                {
                    switch (reading)
                    {
                        case 1:
                            
                            do
                            {
                                Console.WriteLine("Хотите ввести новую персону в список контактов? Если да, введите Y, иначе N:");
                                check = Console.ReadLine().ToUpper();
                            }
                            while (check != "Y" && check != "N");
                            if (check == "Y")
                            {
                                while((person = readRecord())==null)
                                {
                                    Console.WriteLine("Не удалось добавить контакт(введенные данные ошибочны, повторите попытку):");

                                }
                                keyy = phoneList.Count;
                                while (phoneList.ContainsKey(keyy)) keyy++;
                                phoneList.Add(keyy, person);
                            }
                            break;
                        case 2:
                            do
                            {
                                Console.WriteLine("Хотите удалить персону из списка контактов? Если да, введите Y, иначе N:");
                                check = Console.ReadLine().ToUpper();
                            }
                            while (check != "Y" && check != "N");
                            if (check == "Y")
                            {
                                Console.WriteLine("Кого Вы хотите удалить? Введите ФИО или номер в книжке:");

                                string who = Console.ReadLine();
                                int temp;
                                if (int.TryParse(who, out temp)) deleteRecord(temp);
                                else
                                {
                                    string[] fio = new string[3];
                                    fio[0] = ""; fio[1] = ""; fio[2] = "";
                                    fio = who.Split(' ');

                                    foreach  (KeyValuePair<int, Person> item in phoneList)
                                    {
                                        if (fio.Length == 3 && item.Value.Name == fio[1] && item.Value.SecondName == fio[2] && item.Value.Surname==fio[0] ) printRecord(item.Key);
                                    }
                                    Console.WriteLine("Кого из перечисленных Вы хотите удалить? Введите номер в книжке:");
                                    while (!int.TryParse((fio[0]=Console.ReadLine()), out temp))
                                    {
                                        Console.WriteLine("Кого из перечисленных Вы хотите удалить? Введите еще раз, пожалуйста, номер. ");
                                        Console.WriteLine("Выйти в главное меню: введите слово \"конец\":");

                                        if (fio[0].ToLower()=="конец") break;
                                    }
                                    deleteRecord(temp);
                                }
                            }
                            break;
                        case 3:
                            Console.WriteLine("Действительно вывести?(если нет, введите слово \"конец\")");
                            while ( Console.ReadLine().ToLower() != "конец")
                            {
                                if (!printAllRecords())
                                    Console.WriteLine("База пуста!");

                                Console.WriteLine("Выйти в главное меню: введите слово \"конец\":");
                            }
                            break;
                        case 4:
                            check = "";
                            keyy=0;
                            Console.WriteLine("Введите номер:");

                            while ((check=Console.ReadLine().ToLower()) != "конец")
                            {
                                if (int.TryParse(check, out keyy))
                                    if (!printRecord(keyy))
                                        Console.WriteLine("Неправильно введен номер, введите еще раз, или, чтобы");
                                    else { }//printRecord(keyy); }
                                else
                                    Console.WriteLine("Неправильно введен номер, введите еще раз, или, чтобы");
                                Console.WriteLine("Выйти в главное меню: введите слово \"конец\":");
                            }
                            break;
                        case 5:
                            check = "";
                            keyy = 0;
                            Console.WriteLine("Введите ФИО:");

                            while ((check = Console.ReadLine().ToLower()) != "конец")
                            {
                                string[] fio = new string[3];
                                fio[0] = ""; fio[1] = ""; fio[2] = "";
                                fio = check.Split(' ');
                                while(fio.Length > 3|| fio.Length<2)
                                {
                                    fio = (Console.ReadLine()).Split(' ');
                                }
                                if (fio.Length == 2) //чтобы работало, даже если ввели только фамилию и имя
                                { 
                                    fio[0] = fio[0] + " " + fio[1] + " j";
                                    fio = fio[0].Split(' ');
                                    fio[2] = "";
                                }

                                foreach (KeyValuePair<int, Person> item in phoneList)
                                {
                                    if (item.Value.Name.Equals(fio[1]) && item.Value.Surname.Equals(fio[0]))
                                        if (!printRecord(item.Key)) Console.WriteLine("Произошла ошибка!");
                                        
                                }
                                
                                Console.WriteLine("Выйти в главное меню: введите слово \"конец\":");
                            }
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine("\nЧто Вы хотите сделать?\nНомера задач:\n1. Добавить новый контакт.\n" +
                "2. Удалить старый контакт.\n" +
                "3. Вывести всю записную книжку.\n" +
                "4. Вывести контакт по номеру в книжке.\n" +
                "5. Вывести данные контакта по Фамилии и Имени.\n" +
                "6. Выйти(закрыть программу).");
                }
            } while (reading!=6);
            
            /////////////////////////////////////////
            


        }
        
        public static Person readRecord()
        {
           
                long phoneNumber = 0;
                string country = "";
                DateTime birthDay= DateTime.MinValue;
                string organisation = "", post = "", otherData = "";
                bool flagCanIRead = false;

                string[] fio = new string[3];
                fio[0] = ""; fio[1] = ""; fio[2] = "";
                Console.WriteLine("Введите ФИО через пробел:");
                fio = Console.ReadLine().Split(' ');
                while (fio.Length!=3 || fio[0] == "" || fio[1] == "" || fio[2] == "" || fio[0] == " " || fio[1] == " " || fio[2] == " ")
                {
                    Console.WriteLine("Неверно, введите еще раз:");
                    fio = Console.ReadLine().Split(' ');
                }
                    

                Console.WriteLine("Введите телефон без разделительных символов и пробелов(11 цифр - с кодом города, если стационарный):");
                flagCanIRead = false;
                while (!(flagCanIRead = long.TryParse(Console.ReadLine(), out phoneNumber))|| phoneNumber.ToString().Length!=11)
                {
                    Console.WriteLine("Неверно, введите еще раз:");
                }

                Console.WriteLine("Введите название страны:");
                country = Console.ReadLine();
                while (country == "" || country == " ")
                {
                    Console.WriteLine("Неверно, введите еще раз:");
                    country = Console.ReadLine();
                    
                }
                

                string check = "";
                do
                {
                    Console.WriteLine("Вы хотите внести день рождения? Если да, введите Y, иначе N:");
                    check = Console.ReadLine().ToUpper();
                }
                while (check != "Y" && check != "N");
                if (check == "Y")
                {
                    Console.WriteLine("Введите телефон день рождения в формате ДД/ММ/ГГГГ :");
                    birthDay = Convert.ToDateTime(Console.ReadLine()).Date;
                }

                check = "";
                do
                {
                    Console.WriteLine("Вы хотите внести организацию? Если да, введите Y, иначе N:");
                    check = Console.ReadLine().ToUpper();
                }
                while (check != "Y" && check != "N");
                if (check == "Y")
                {
                    Console.WriteLine("Введите описание организации:");
                    organisation = Console.ReadLine();
                }

                check = "";
                do
                {
                    Console.WriteLine("Вы хотите внести должность? Если да, введите Y, иначе N:");
                    check = Console.ReadLine().ToUpper();
                }
                while (check != "Y" && check != "N");
                if (check == "Y")
                {
                    Console.WriteLine("Введите описание должности:");
                    post = Console.ReadLine();
                }
                check = "";
                do
                {
                    Console.WriteLine("Вы хотите внести дополнительную информацию? Если да, введите Y, иначе N:");
                    check = Console.ReadLine().ToUpper();
                }
                while (check != "Y" && check != "N");
                if (check == "Y")
                {
                    Console.WriteLine("Если Вам есть что добавить, вы можете ввести это далее:");
                    otherData = Console.ReadLine();
                }
                if (fio[0] != "" && fio[1] != "" && fio[2] != "" && phoneNumber != 0) 
                {
                    Person pers = new Person(fio[1], fio[0], fio[2], phoneNumber, country, birthDay, organisation, post, otherData);    
                    return pers; 
                }
                else return null;
            
        }

        public static bool printShortRecord(int key)
        {
            if (phoneList.ContainsKey(key))
            {
                Person temp;
                phoneList.TryGetValue(key, out temp);
                Console.WriteLine("ID: " + key);
                Console.Write(temp.ToShortString());
                return true;
            }
            else return false;


        }

        public static bool printRecord(int key)
        {
            if (phoneList.ContainsKey(key))
            {
                Person temp;
                phoneList.TryGetValue(key, out temp);
                Console.WriteLine("ID: "+key);
                Console.Write(temp.ToString());

                return true;
            }
            else return false;


        }
        public static bool printAllRecords()
        {
            if (phoneList.Count == 0) return false;
            else
            {
                foreach (KeyValuePair<int, Person> item in phoneList)
                {
                    printShortRecord(item.Key);
                }
                return true;
            }
        }
        public static bool changeRecord(int key, Person newData)
        {
            if (phoneList.ContainsKey(key)) {
                    if(phoneList.Remove(key))
                    {
                        phoneList.Add(key, newData);
                        return true;

                    }
                return false;
            }
            else return false;

        }
        public static bool deleteRecord(int key)
        {
            if (phoneList.ContainsKey(key))
            {
                phoneList.Remove(key);
                Console.WriteLine("Успешно удалена запись с номером: "+key);
                return true;
            }
            else return false;
        }

    }

    class Person {
        string name, surname, secondName;

        long phoneNumber;
        string country;
        DateTime birthDay;
        string organisation = "", post="", otherData="";

        public override string ToString()
        {
            string res="_____________________________________________________\n";

            res += "Имя: \t\t\t\t" + this.Surname+" "+this.Name+" "+this.SecondName+"\n";
            res += "День рождения: \t\t\t" + ((this.BirthDay == DateTime.MinValue) ?"N/A" :this.BirthDay.Date.ToLongDateString() )+ "\n";
            res += "Номер телефона: \t\t" + this.PhoneNumber+"\n";
            res += "Страна: \t\t\t" + this.Country + "\n";
            res += "Организация, должность: \t" + ((this.Organisation!=""&&this.Organisation != " ") ? this.Organisation:"N/A")+ " \t" + ((this.Post != "" && this.Post != " ") ? this.Post : "N/A") + "\n";
            res += "Прочее: \t\t\t" + ((this.OtherData != "" && this.OtherData != " ") ? this.OtherData : "N/A") + "\n";
            res += "_____________________________________________________\n";
            return res;
        }

        public string ToShortString()
        {
            string res = "_____________________________________________________\n";

            res += "Имя: \t\t\t" + this.Surname + " " + this.Name + "\n";
            res += "Номер телефона: \t" + this.PhoneNumber + "\n";
            res += "_____________________________________________________\n";
            return res;
        }

        public Person(string name, string surname, string secondName, long phoneNumber, string country, DateTime birthDay, string organisation, string post, string otherData)
        {
            this.Name = name;
            this.Surname = surname;
            this.SecondName = secondName;
            this.PhoneNumber = phoneNumber;
            this.Country = country;
            this.BirthDay = birthDay;
            this.Organisation = organisation;
            this.Post = post;
            this.OtherData = otherData;
        }

        public Person(string name, string surname, string secondName, long phoneNumber, string country)
        {
            this.Name = name;
            this.Surname = surname;
            this.SecondName = secondName;
            this.PhoneNumber = phoneNumber;
            this.Country = country;

            this.BirthDay = DateTime.Now;
            this.Organisation = "";
            this.Post = "";
            this.OtherData = "";
        }

        public string Name
        {
            get; set;
        }
        public string Surname
        {
            get; set;
        }
        public string SecondName
        {
            get; set;
        }
        public long PhoneNumber
        {
            get; set;
        }
        public string Country
        {
            get; set;
        }

        public DateTime BirthDay
        {
            get {
                return birthDay.Date;
            } set
            {

                birthDay = value.Date;
            }
        }
        public string Organisation
        {
            get; set;
        }
        public string Post
        {
            get; set;
        }
        public string OtherData
        {
            get; set;
        }
    }

    

}

